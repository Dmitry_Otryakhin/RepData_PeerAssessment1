Reproducible Research: Peer Assessment 1
========================================
#### Loading and preprocessing ############################

In this part of code the raw data is loaded and preprocessed. Date and time is encoded to POSIXct; created character time format

```{r}
Prep_data<-function() {

  Raw_data<-read.csv("activity.csv")

  raw_time<-Raw_data$interval

  hour<-(raw_time-raw_time %% 100)/100
  minute<-raw_time%%100
  formated_t<-paste(Raw_data$date," ", hour, ":", minute, sep="")
  clean_t<-strptime(formated_t,"%Y-%m-%d %H:%M")
  char_t<-format(clean_t, "%H:%M")
  Preprocessed<-cbind(Raw_data, clean_t, char_t)

}

```  
  
  
  
#### What is mean total number of steps taken per day? ##############

Here, I calculate total number of steps for all days within the data.
```{r}
Sum_steps<-function() {
  
  table<-Prep_data()
  NA_index<-complete.cases(table)
  table<-table[NA_index, ]
  sum_table<-aggregate(table$steps, by=list(table$date), FUN= sum)

}

sum_table<-Sum_steps()
sum_tablee<-sum_table
names(sum_tablee)<-c("date", "Total steps")
sum_tablee

```


```{r}

  hist(sum_table$x, main="Total steps number per day", xlab="steps number", col=4)
  mean_steps<-mean(sum_table$x)
  med_steps<-median(sum_table$x)
  
```

The mean and the median of steps taking per day are `r as.integer(mean_steps)` (rounded off) and `r med_steps`.

#### What is the average daily activity pattern? ##############

This is the time series plot of the 5-minute interval (x-axis) and the average number of steps taken, averaged across all days (y-axis)
```{r}
Daily_pattern<- function() {
  
  table<-Prep_data()
  NA_index<-complete.cases(table)
  table<-table[NA_index, ]
  Interval_table<-aggregate(table$steps, by=list(table$char_t), FUN= mean)
  Interval_table$Group.1<-strptime(Interval_table$Group.1, "%H:%M")
  Interval_table
  
}

  t<-Daily_pattern()
  plot(t$Group.1, t$x, type="l", col="green", main="Average daily activity pattern", xlab="Time Interval", ylab="Mean steps")


```



```{r}
m<-max(t$x)
max_s_int<-format(t$Group.1[t$x==m],"%H:%M")

```
We can see, that 5-minute interval with the maximum steps amount is `r max_s_int`. The maximum is `r m`.

#### Inputting missing values ##############
```{r}
table<-Prep_data()
NA_index<-complete.cases(table)
Count_NA<-sum(!NA_index)
```
Total amount of rows containing missing values is `r Count_NA`  
As a strategy for inputting missing values I chosed mean values of 5-minute intervals, avereged across the same intervals of all days without missing values.  
Here, I create a new dataset which is equal to original one, but with missing values filled in.
```{r}
Input<-function(table) {
  
  New_table<-table
  tt<-t 
  names(tt)<-c("char_t", "steps")
  
  tt$char_t<-format(tt$char_t, "%H:%M")
  na_new<-New_table[!NA_index,]
  
  for(i in 1:nrow(tt)) {
    st<-tt$steps[i]
    t<-tt$char_t[i]
    na_new$steps[na_new$char_t==t]<-st
    
  }
  
  for(i in 1:nrow(na_new)) {  
    New_table$steps[New_table$clean_t==na_new$clean_t[i]]<-na_new$steps[i]
  }
  
  New_table  
  
}  

fixed_table<-Input(table)
sum_f_table<-aggregate(fixed_table$steps, by=list(table$date), FUN= sum)
names(sum_f_table)<-c("char_t", "steps")
hist(sum_f_table$steps, main="Total steps number per day, fixed", xlab="steps number", col=4)
```
A Calculation of the mean and the median values of the new dataset looks like that:
```{r}

mean_steps_fix<-mean(sum_f_table$steps)
med_steps_fix<-median(sum_f_table$steps)

```
Mean steps amount is `r mean_steps_fix`, median is `r med_steps_fix`.  
As we can see, these numbers don't differ a lot from that of in the first part of assignment. Histograms are similar too.


#### Are there differences in activity patterns between weekdays and weekends?
```{r}
Sys.setlocale(locale="English")

Weekday_factor<- function() {

    fixed_table$day<-weekdays(fixed_table$clean_t)
    fixed_table$num_day<-as.factor(fixed_table$day)
    levels(fixed_table$num_day)<-c(5,1,6,7,4,2,3)
    fixed_table$num_day<-as.integer(fixed_table$num_day)
    fixed_table$ind<-"weekday"
    fixed_table$ind[fixed_table$num_day>5]<-"weekend"
    fixed_table
    
}

fix_t<-Weekday_factor()
  
    library(plyr)
    sum_t<-ddply(fix_t, .(ind, char_t), summarise, mean=mean(steps))
    sum_t$char_t<-strptime(sum_t$char_t, "%H:%M")
    
    fs<-sum_t$ind
    library(ggplot2)
    q<-qplot(sum_t$char_t, sum_t$mean, sum_t, geom="path", facets = . ~ fs, main=" weekday's and weekend's activity patterns")
    q<-q+labs(x="Time", y= "Steps mean")
    q

```  

This panel shows us some patterns in activity. We can see, that a lot of steps amassed in the time interval between 06:00 and 24:00 on both graphics. The maximun amount of steps on weekday and on weekend are the same and appears at the same time - about 09:00. There some differences between weekdays and weekends. There is a local maximum about 06:00 on weekend, which doesn't appear on weekday.  

